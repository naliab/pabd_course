import unittest

from requests import request


PARAMS = '?total_meters=1000'


class MyTestCase(unittest.TestCase):
    def setUp(self) -> None:
        self.url = 'http://localhost:5000/'
        self.endpoint = 'predict'

    def test_home_page(self):
        response = request('GET', self.url)
        self.assertIn('Используйте /predict?total_meters=<число> \
для получения прогноза.', response.text)

    def test_predict(self):
        response = request('GET', self.url + self.endpoint + PARAMS)
        self.assertIn('Спрогнозированная цена для 1000 метров = \
190 тыс. руб.', response.text)
