"""House price prediction service"""
from joblib import load

import yaml
from flask import Flask, request
from yaml import SafeLoader

app = Flask(__name__)
config = yaml.load(open('params/predict_app.yaml'), SafeLoader)
model = load(config['model_path'])


@app.route("/")
def home():
    return 'Используйте /predict?total_meters=<число> для получения прогноза.'


@app.route("/predict")
def predict():
    """Dummy service"""
    f_1 = request.args.get("total_meters")
    result = model.predict([[int(f_1)]])[0]
    return f'Спрогнозированная цена для {int(f_1)} метров = \
{round(result / 1000)} тыс. руб.'


if __name__ == "__main__":
    app.run(host='0.0.0.0')
