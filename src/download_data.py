from requests import get  # to make GET request


def download(url, file_name):
    # open in binary mode
    with open(file_name, "wb") as file:
        # get request
        response = get(url)
        # write to file
        file.write(response.content)


if __name__ == '__main__':
    url = 'https://storage.yandexcloud.net/pabd/ \
cian_parsing_result_sale_1_42_moskva_01_Jun_2023_21_44_30_521694.csv'
    file_name = 'data/raw/cian/' + url.split('/')[-1]
    download(url, file_name)
