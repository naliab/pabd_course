#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os

import boto3
from dotenv import load_dotenv

BUCKET_NAME = 'naliab-s3'

load_dotenv('../.env_lab')
aws_access_key_id = os.getenv('aws_access_key_id')
aws_secret_access_key = os.getenv('aws_secret_access_key')
print(aws_access_key_id)

session = boto3.session.Session()
s3 = session.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net',
    aws_access_key_id=aws_access_key_id,
    aws_secret_access_key=aws_secret_access_key
)

s3.upload_file('../models/model_1.joblib', 'pabd-s3', 'model_1.joblib')
