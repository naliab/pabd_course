"""Preprocess cian data.
Takes params from params.yaml """
import os.path
import shutil
from pathlib import Path
import random

import pandas as pd
import yaml
from sklearn.model_selection import train_test_split

IN_DATA = 'data/raw/cian'
OUT_DIR = 'data/processed'
TRAIN_OUT = os.path.join(OUT_DIR, 'train.csv')
TEST_OUT = os.path.join(OUT_DIR, 'test.csv')
params = yaml.safe_load(open("params.yaml"))["preprocess"]
random.seed(params["seed"])


def read_cian_data() -> pd.DataFrame:
    csv_files = Path(IN_DATA).glob('*.csv')
    df_list = [pd.read_csv(f, sep=';', encoding='cyrillic') for f in csv_files]

    df = pd.concat(df_list)
    columns = ['total_meters', 'price']
    return df[columns]


if __name__ == '__main__':
    df = read_cian_data()

    df['price'] = df['price'] / 1000
    df = df.drop(df[df.total_meters < 0].index)

    train_df, test_df = train_test_split(df, test_size=params['split'])
    if os.path.exists(OUT_DIR):
        shutil.rmtree(OUT_DIR)
    os.mkdir(OUT_DIR)
    train_df.to_csv(TRAIN_OUT, index=False)
    test_df.to_csv(TEST_OUT, index=False)
